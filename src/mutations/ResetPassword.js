import { gql } from "apollo-boost";

const RESET_PASSWORD_MUTATION = gql`
  mutation reset_password_email($otp: String!, $password: String!) {
    resetPassword(otp: $otp, newPassword: $password) {
      status
      message
    }
  }
`;

export { RESET_PASSWORD_MUTATION };
