import { gql } from "apollo-boost";

export const CREATE_NEW_INSPECTION_RECORD_MUTATION = gql`
  mutation create_new_inspection_record($data: NewInspectionRecordInput!) {
    createNewInspectionRecord(data: $data) {
      status
      message
    }
  }
`;
