import { gql } from "apollo-boost";

export const CREATE_QAO_LOG_MUTATION = gql`
  mutation create_user_log($plant: String!, $log: String!, $title: String!) {
    createUserLog(data: { plant: $plant, log: $log, title: $title }) {
      id
      plant
      created_at
      title
      log
    }
  }
`;
