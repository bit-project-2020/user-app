import { gql } from "apollo-boost";

export const REQUEST_PASSWORD_RESET_MUTATION = gql`
  mutation request_password_reset($email: String) {
    requestPasswordReset(email: $email) {
      status
      message
    }
  }
`;
