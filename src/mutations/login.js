import { gql } from "apollo-boost";

export const LOGIN_MUTATION = gql`
  mutation login($email: String!, $password: String!) {
    login(email: $email, password: $password) {
      id
      first_name
      last_name
      role
      department
      user_name
      email
    }
  }
`;
