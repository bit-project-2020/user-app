import React from "react";
import { ApolloProvider } from "@apollo/react-hooks";
import ApolloClient from "apollo-boost";

import SessionRouter from "./routes/SessionRouter";
import AppRouter from "./routes/AppRouter";
import { GRAPHQL_API_ENDPOINT } from "./config";
import "./App.css";
// import logo from './logo.svg';
import User from "./components/User";

const client = new ApolloClient({
  uri: GRAPHQL_API_ENDPOINT,
  credentials: "include",
  headers: {
    AppName: "User",
  },
});

function App() {
  return (
    <div className="App">
      <ApolloProvider client={client}>
        <User>
          {(data) => {
            // u can use here to check if there is a plant already in the session
            // console.log('current user query data output',data);
            //console.log(data.currentUser)
            // if(data.currentUser){
            if (data && data.currentUser) {
              return <AppRouter />;
            } else {
              return <SessionRouter />;
            }
          }}
        </User>
      </ApolloProvider>
    </div>
  );
}

export default App;
