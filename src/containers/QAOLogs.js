import React, {useState, useEffect} from "react";
import CustomHeader from "../components/CustomHeader";
import { LOGS_QUERY } from "../queries/logs";
import { useQuery } from "@apollo/react-hooks";
import { getCookieByName } from "../lib/getCookieByName";
import styled from "styled-components";
import {
  Table,
  Button,
  Space,
  Form,
  DatePicker,
  Input,
  Modal
} from "antd";
import {UserViewSection} from "../components/dashboard/UserLogsSection";
import {TableActions} from "../components/shared/StyledComponent";
import moment from "moment";
import showNotifications from "../lib/showNotifications";
import QAODetail from "../components/QAODetail";

const { Search } = Input;

const LogsWrapper = styled.div`
  padding: 15px;

  .user-info {
    display: flex;
    img.user-photo-small-round {
      width: 30px;
      height: 30px;
      border-radius: 2rem;
    }
  }

  div.log-info {
    display: flex;
    justify-content: space-around;
  }

  span.shift {
    background-color: #f8f8f8;
    padding: 5px;
  }

  #top {
    padding: 1rem;
    display: flex;
    justify-content: space-between;
    h1 {
      font-weight: bold;
      font-size: 1.2rem;
      margin-bottom: 0px;
    }
    h3 {
      font-size: 0.8rem;
    }
    button {
      color: #3e92ef;
      border-color: #3e92ef;
      border-radius: 5px;
      margin-top: 0.5rem;
    }
  }

  .log-card {
    margin: 0.5rem;
    padding: 0.5rem;
    background-color: #fff;
  }
`;

const columns = (setInitialLog) => [
  {
    title: "Logs",
    dataIndex: "actions",
    align: "center",
    render(txt, log, index) {
      const date_and_time = new Date(log.created_at);
      return (
          <div className="log-card" key={log.id} onClick={() => {setInitialLog(log)}}>
            <div>
              <h4>
                <b> {log.title}</b>
              </h4>
              <div className="log-info">
                <span className="shift"> {log.shift} </span>
                <UserViewSection log={log} />
                <div>
                  {date_and_time.toLocaleDateString()}
                  {date_and_time.toLocaleTimeString()}
                </div>
              </div>
            </div>
          </div>
      );
    },
  },
];

const QAOLogs = () => {

  const today = new Date();

  let sevenDaysBefore = new Date();
  sevenDaysBefore.setDate(sevenDaysBefore.getDate() - 7);

  const [searchValue, setSearchValue] = useState("");
  const [startDate, setStartDate] = useState(moment(sevenDaysBefore));
  const [endDate, setEndDate] = useState(moment(today));
  const [initialLog, setInitialLog] = useState(null);

  useEffect(() => {
    refetch()
      });

  const { loading, error, data, refetch } = useQuery(LOGS_QUERY, {
    variables: {
      plant: getCookieByName("plant"),
      start: startDate,
      end: endDate
    },
  });
  if (error) {
    return <div>error</div>;
  }
  if (loading) {
    return <div> loading</div>;
  }
  if (data) {
    let logs = data.logs;
    // filtered data
    logs = logs.filter((log) =>
      log.title.toLowerCase().includes(searchValue.toLowerCase())
    );

    return (
      <div>
        <CustomHeader />
        <LogsWrapper>
          <h1>QAO Logs</h1>

          <div>
            <span>Filtering option section</span>

            <div>
              <h1> Search Results Section</h1>
            </div>
            <TableActions>
          <div className="search-box-wrapper">
            <Search
              value={searchValue}
              className="search-box"
              placeholder="Search by log title"
              onChange={(event) => {
                const { value } = event.target;
                setSearchValue(value);
              }}
            />
          </div>
          </TableActions>
            <Space direction={"horizontal"}>
        <Form.Item label="Start Date">
          <DatePicker
            allowClear={false}
            onChange={(date, dateString) => {
              if (endDate.diff(date, "days") < 0) {
                showNotifications({
                  type: "error",
                  message: "Please select a date before the end date.",
                });
              } else {
                setStartDate(date);
              }
            }}
            value={startDate}
          />
        </Form.Item>

        {/* for product */}
        <Form.Item label="End Date">
          <DatePicker
            allowClear={false}
            onChange={(date, dateString) => {
              if (startDate.diff(date, "days") > 0) {
                showNotifications({
                  type: "error",
                  message: "Please select a date before the end date.",
                });
              } else {
                setEndDate(date);
              }
            }}
            value={endDate}
          />
        </Form.Item>
      </Space>
            <div>
            <Table
          loading={loading}
          bordered
          rowKey={(log) => log.id}
          columns={columns(setInitialLog)}
          dataSource={logs}
        />
      </div>
          </div>
        </LogsWrapper>
        <Modal
          title="QAO Details"
          visible={initialLog !== null}
          onCancel={() => {
            setInitialLog(null)
          }}
          onOk={() => {
            console.log("On OK Pressed")
          }}
          footer={null}
        >
          <QAODetail log={initialLog}/>
        </Modal>
      </div>
    );
  }
};

export default QAOLogs;