import React from 'react';
import styled from 'styled-components';

const SessionComponent = styled.div`
  height: 100vh;
  display: flex;
  align-items: center;
  justify-content: center;
  text-align: center;

  .session-form-button {
    margin-bottom: -24px;
    display: block;
    padding-left: 40px;
    padding-right: 40px;
    margin-left: auto;
    margin-right: auto;
  }

  .session-form-card {
    width: 450px;
  }
`;

const SessionLayout = ({ children }) => <SessionComponent>{children}</SessionComponent>;

export default SessionLayout;
