/* eslint-disable jsx-a11y/alt-text */
import React, { useState, useEffect } from "react";

import { useQuery } from "@apollo/react-hooks";

import { PRODUCT_SPECIFICATIONS_QUERY } from "../queries/productSpecifications";

import { useParams } from "react-router-dom";

import { Radio, Space, Table } from "antd";

import styled from "styled-components";

const SpecificationsWrapper = styled.div`
  padding: 10px;

  img {
    max-width: 170px;
    margin: 10px;
    border: 0.8px solid #3e92ef;
    padding: 2px;
    border-radius: 5px;
  }
`;

const columns = () => [
  {
    title: "Parameter Name",
    dataIndex: "parameter",
    key: "parameter",
  },
  {
    title: "specification",
    dataIndex: "specification",
    key: "specification",
    render: (value, record) => {
      // console.log(66502200, record);
      switch (record.parameter_type) {
        case "BOOLEAN":
          if (value !== undefined && value !== null) {
            return <div> {value === "true" ? "Ok" : "Not Ok"}</div>;
          } else {
            return <div> no specification </div>;
          }
        case "NUMBER":
          if (value !== undefined && value !== null) {
            return (
              <div>
                <span>
                  min: {value.split(",")[0]} {record.unit}&nbsp;&nbsp;&nbsp;
                </span>
                <span>
                  max: {value.split(",")[1]} {record.unit}
                </span>
              </div>
            );
          } else {
            return <div> no specification </div>;
          }
        case "NUMBER_EXACT":
          if (value !== undefined && value !== null) {
            return (
              <div>
                {value} {record.unit}
              </div>
            );
          } else {
            return <div> no specification </div>;
          }

        default:
          return <div> N/A </div>;
      }
    },
  },
];

const Specifications = () => {
  const { product_id, size_id } = useParams();
  const [parameterMode, setParameterMode] = useState("PROCESS");

  const { loading, error, data, refetch } = useQuery(
    PRODUCT_SPECIFICATIONS_QUERY,
    {
      variables: {
        product_id: parseInt(product_id),
        product_size: parseInt(size_id),
        spec_type: parameterMode,
      },
    }
  );

  useEffect(() => {
    refetch();
  }, [parameterMode, refetch]);

  if (loading) {
    return <div>Loading</div>;
  }

  if (error) {
    return <div>Something went wrong</div>;
  }

  if (data) {
    //console.log(400200300, data);
    const { product, size, plant_param_with_spec } = data.productSpecifications;
    return (
      <SpecificationsWrapper>
        <div>
          <h2>Product : {product && product.name}</h2>
          <h2>Size : {size && size.size_name}</h2>
        </div>
        <Space>
          Specification type:
          <span>
            <Radio.Group
              onChange={(event) => {
                // console.log(event);
                setParameterMode(event.target.value);
              }}
              value={parameterMode}
            >
              <Radio value="PROCESS">PROCESS</Radio>
              <Radio value="PACKAGING">PACKAGING</Radio>
            </Radio.Group>
          </span>
        </Space>

        <div>
          {/* lets render the product here */}
          <img src={product && product.image_url}></img>

          <div id="spec-table">
            <Table
              loading={loading}
              bordered
              rowKey={(record) => record.parameter}
              columns={columns()}
              dataSource={plant_param_with_spec}
              pagination={false}
            ></Table>
          </div>
        </div>
      </SpecificationsWrapper>
    );
  }
};

export default Specifications;
