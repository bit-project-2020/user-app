import React from "react";
import styled from "styled-components";

import User from "../components/User";

import UserLogsSection from "../components/dashboard/UserLogsSection";
import DeviationsSection from "../components/dashboard/DeviationsSection";
import UserCard from "../components/dashboard/UserCard";
// import showNotifications from "../lib/showNotifications";

const DashboardStyles = styled.div``;

const Dashboard = () => {
  return (
    <DashboardStyles>
      <User>
        {(data) => {
          if (data) {
            return (
              <>
                <UserCard user={data.currentUser} />
                <UserLogsSection />
                <DeviationsSection />
              </>
            );
          }
        }}
      </User>
    </DashboardStyles>
  );
};

export default Dashboard;
