import React, { useState, useEffect } from "react";
import { useLocation, Link } from "react-router-dom";
import { useQuery, useLazyQuery } from "@apollo/react-hooks";

import styled from "styled-components";

import { PRODUCTS_QUERY } from "../queries/products";
import { PRODUCT_SIZES_QUERY } from "../queries/sizes";
import { getCookieByName } from "../lib/getCookieByName";
import { Button } from "antd";

import { ArrowLeftOutlined } from "@ant-design/icons";

import defaultImage from "../assets/images/default-image.png";

const ProductBoxWrapper = styled.div`
  padding: 10px;

  h2.section-title {
    font-weight: bold;
    font-size: 1.2rem;
    font-family: sans-serif;
  }
  .product-list {
    display: flex;
    justify-content: space-around;
  }

  .size-boxes-wrapper {
    display: flex;
    flex-direction: column;
    flex-grow: unset;
  }
`;

const ProductBox = styled.div`
  height: 150px;
  border: 3px solid #3e92ef;
  border-radius: 15px;
  width: 150px;
  display: inline-block;
  margin: 10px;
  text-align: center;
  overflow: hidden;

  .product-name {
    font-weight: bold;
    /* font-size: 0.9rem; */
  }

  img {
    width: 75%;
  }
`;

const SizeBox = styled.div`
  border: 1px solid #3e92ef;
  border-radius: 5px;
  background-color: ivory;
  width: 94%;
  display: inline-block;
  margin: 10px;
  text-align: center;
  overflow: hidden;
  min-height: 50px;
  box-sizing: border-box;
  font-size: 1.5rem;
  margin-top: 0rem;

  &:hover {
    background-color: #fff;
    font-size: 1.55rem;
    transition: all 0.2s;
  }
`;

export default function PlantProductCards() {
  let location = useLocation();

  // view spec boolean to detect where to redirect
  const is_view_specs = location.search.includes("viewspecs=1");

  // we can use the following parameter when we pass data to the next route.
  let isProcessData =
    location.pathname === "/new-process-record" ? true : false;

  const plant = getCookieByName("plant");
  if (!plant) {
    // TODO logout the user
  }
  const [product, setProduct] = useState(null);
  const [sizes, setSizes] = useState([]);

  const {
    loading: productsQueryLoading,
    // error: productsQueryError,
    data: productsQueryData,
  } = useQuery(PRODUCTS_QUERY, {
    variables: {
      where: {
        plant,
      },
    },
  });

  const [
    fetchSizesQuery,
    {
      loading: loadingSizes,
      data: SizesData,
      //error: SizesError
    },
  ] = useLazyQuery(PRODUCT_SIZES_QUERY);

  useEffect(() => {
    if (!fetchSizesQuery) return;

    fetchSizesQuery({
      variables: {
        where: {
          product_id: product && product.id,
        },
      },
    });

    if (SizesData) {
      const activeSizes = SizesData.sizes.filter(
        (size) => size.is_active === true
      );
      setSizes(activeSizes);
      // lets filter sizes and show only isactive
    }
  }, [product, fetchSizesQuery, SizesData]);

  if (productsQueryLoading || loadingSizes) {
    return <h1>loading</h1>;
  }

  if (productsQueryData) {
    const { products } = productsQueryData;

    return (
      <ProductBoxWrapper>
        {!product && (
          <>
            <h2 className="section-title">
              {!is_view_specs && products.length !== 0
                ? `Select a product for enter ${
                    isProcessData ? "Process" : "Packaging"
                  } data`
                : is_view_specs
                ? "Select Product to view specifications"
                : "No products yet"}
              {/* {isProcessData ? "Process" : "Packaging"}  */}
            </h2>
            <div className="product-list">
              {products.map((element) => {
                return (
                  <ProductBox
                    onClick={() => {
                      setProduct(element);
                    }}
                    key={element.id}
                  >
                    <div className="product-name">{element.name}</div>
                    <img
                      alt="product"
                      src={element.image_url || defaultImage}
                    />
                  </ProductBox>
                );
              })}
            </div>
          </>
        )}
        {product && SizesData && (
          <>
            <h2 className="section-title">Select a size of {product.name}</h2>
            <br />
            <div className="size-boxes-wrapper">
              {sizes.map((size) => {
                return (
                  <div className="size-box">
                    <Link
                      to={`/${
                        is_view_specs
                          ? "specifications"
                          : isProcessData
                          ? "create-process-record"
                          : "create-packaging-record"
                      }/${product.id}/${size.id}`}
                      key={size.id}
                    >
                      <SizeBox> {size.size_name}</SizeBox>
                    </Link>
                  </div>
                );
              })}
            </div>

            <Button
              size="large"
              type="primary"
              onClick={() => {
                setProduct(null);
              }}
            >
              <ArrowLeftOutlined />
              Change Product
            </Button>
          </>
        )}
      </ProductBoxWrapper>
    );
  }
}
