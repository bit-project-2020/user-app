import React from "react";
import { useMutation, useQuery } from "@apollo/react-hooks";

import showNotifications from "../lib/showNotifications";
import { MailOutlined, KeyOutlined } from "@ant-design/icons";

import { CURRENT_USER_QUERY } from "../components/User";
import { PLANTS_QUERY } from "../queries/plants";

import LoadingScreen from "../components/LoadingScreen";

import { Input, Button, Card, Form as AntForm, Select } from "antd";
import { Formik } from "formik";
import * as Yup from "yup";
import ChandimaSweetsLogo from "../assets/images/logo.svg";
import { LOGIN_MUTATION } from "../mutations/login";

const { Option } = Select;
const FormItem = AntForm.Item;

const LoginSchema = Yup.object().shape({
  email: Yup.string().required("Please enter your email."),
  password: Yup.string().required("Please enter your password."),
  selectedPlant: Yup.string().required("please enter a plant"),
});

const Login = () => {
  const { loading: plantsLoading, error, data } = useQuery(PLANTS_QUERY);

  const [logUserIn] = useMutation(LOGIN_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Login Succesful!" });
    },
    onError: (error) => {
      let message = error.message.split(",")[0];
      message = message.split("GraphQL error:")[1];

      showNotifications({
        type: "error",
        message: `Login Failed! ${message}`,
      });
    },
    refetchQueries: [{ query: CURRENT_USER_QUERY }],
  });

  if (plantsLoading) {
    return <LoadingScreen />;
  }

  if (error) {
    console.log(error);
  }

  if (data) {
    // filter isactive plants only
    // console.log(44444, data);
    data.plants = data.plants.filter((plant) => plant.is_active === true);

    return (
      <div>
        <Card
          style={{
            maxWidth: "350px",
          }}
          className="session-form-card"
          title={
            <div className="logo">
              <img
                id="company-logo"
                src={ChandimaSweetsLogo}
                alt="chandima sweets logo"
              />
              <br />
              🙋‍ Welcome! Please Login
            </div>
          }
          actions={[
            <a className="login-form-forgot" href="/reset-password">
              Forgot password
            </a>,
          ]}
        >
          <Formik
            initialValues={{
              email: "",
              password: "",
              selectedPlant: data && data.plants[0].plant_name,
            }}
            validationSchema={LoginSchema}
            onSubmit={async (values, { setSubmitting }) => {
              // create a new cookie to store the plant
              const d = new Date();
              d.setTime(d.getTime() + 30 * 24 * 60 * 60 * 1000);
              const expires = "expires=" + d.toUTCString();
              document.cookie = `plant=${values.selectedPlant}; expires=${expires}`;
              // log the user in
              await logUserIn({
                variables: {
                  ...values,
                },
              });
              setSubmitting(false);
            }}
          >
            {({
              handleSubmit,
              isSubmitting,
              handleBlur,
              handleChange,
              errors,
              values,
            }) => (
              <form onSubmit={handleSubmit}>
                <FormItem
                  validateStatus={errors.email ? "error" : "validating"}
                  help={errors.email}
                >
                  <Input
                    name="email"
                    // prefix={<Icon type="mail" style={{ color: 'rgba(0,0,0,.25)' }} />}
                    prefix={<MailOutlined />}
                    placeholder="Email"
                    value={values.email}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </FormItem>
                <FormItem
                  validateStatus={errors.password ? "error" : "validating"}
                  help={errors.password}
                >
                  <Input
                    name="password"
                    prefix={<KeyOutlined />}
                    type="password"
                    placeholder="Password"
                    value={values.password}
                    onChange={handleChange}
                    onBlur={handleBlur}
                  />
                </FormItem>

                <FormItem
                  validateStatus={errors.selectedPlant ? "error" : "validating"}
                  required={true}
                >
                  <Select
                    style={{ width: 150 }}
                    name="selectedPlant"
                    defaultValue={data.plants[0].plant_name}
                    placeholder="Select the plant"
                    required={true}
                    onChange={(value) => {
                      values.selectedPlant = value;
                    }}
                  >
                    {data.plants.map((plant) => {
                      return (
                        <Option key={plant.plant_name} value={plant.plant_name}>
                          {plant.plant_name}
                        </Option>
                      );
                    })}
                  </Select>
                </FormItem>

                <FormItem>
                  <Button
                    type="primary"
                    htmlType="submit"
                    disabled={isSubmitting}
                    className="session-form-button"
                    loading={isSubmitting}
                  >
                    Log in
                  </Button>
                </FormItem>
              </form>
            )}
          </Formik>
        </Card>
      </div>
    );
  }
};

export default Login;
