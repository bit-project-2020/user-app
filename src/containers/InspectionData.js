import React, { useState, useEffect } from "react";
import { useLazyQuery } from "@apollo/react-hooks";

import { INSPECTION_DATA_QUERY } from "../queries/inspectionData";

import { PRODUCT_SIZES_QUERY } from "../queries/sizes";
import { PRODUCTS_QUERY } from "../queries/products";
import styled from "styled-components";
import { getCookieByName } from "../lib/getCookieByName";

import showNotifications from "../lib/showNotifications";

import moment from "moment";

import {
  useLocation,
  //useHistory
} from "react-router-dom";

import { Table, Space, Select, Button, Form, DatePicker } from "antd";

const InspectionDataWrapper = styled.div`
  padding: 10px;
  .highlight-in-red {
    color: red;
  }
  .ant-select {
    min-width: 100px;
  }

  label {
    font-weight: bold;
  }

  .ant-row.ant-form-item {
    margin-bottom: 0px;
  }

  .inactive {
    color: red;
  }

  .active {
    color: green;
  }
`;

const { Option } = Select;

const columns = ({ structure, plant, pathname }) => {
  const extra_fields = (structure && structure.extra_fields) || [];

  extra_fields.forEach((field) => {
    if (field.type === "IMAGE") {
      return;
    }
  });

  return [
    {
      title: "Date",
      dataIndex: "date",
      key: "date",
      render(text, record) {
        const now = new Date(text);
        return (
          <>
            {now.toLocaleDateString("en-GB")} {now.toLocaleTimeString()}
          </>
        );
      },
    },
    {
      title: "Defective",
      dataIndex: "is_deviated",
      key: "is_deviated",

      render(text, record) {
        if (record.is_deviated) {
          return <div className="inactive"> Yes </div>;
        } else {
          return <div className="active"> No</div>;
        }
      },
    },
    // handle actions field here
    {
      title: "Actions",
      dataIndex: "actions",
      key: "actions",
      align: "center",
      render(text, record) {
        return (
          <Button
            onClick={() => {
              //history.push("/inspectionData");

              // window.open function returns an object you can use its focus() method like below
              // win.focus();
              window.open(
                `${pathname}/${getCookieByName("plant")}/${record.id}`,
                "_blank"
              );
            }}
          >
            view in detail
          </Button>
        );
      },
    },
  ];
};

const InspectionData = () => {
  let location = useLocation();
  const today = new Date();

  // we can get the plant from the path,

  let sevenDaysBefore = new Date();
  sevenDaysBefore.setDate(sevenDaysBefore.getDate() - 7);

  const [startDate, setStartDate] = useState(moment(sevenDaysBefore));
  const [endDate, setEndDate] = useState(moment(today));

  // let history = useHistory();
  const { pathname } = location;

  const [product, setProduct] = useState();
  const [size, setSize] = useState();

  const [fetchInspectionData, { loading, error, data }] = useLazyQuery(
    INSPECTION_DATA_QUERY,
    {
      variables: {
        where: {
          plant: getCookieByName("plant"),
          type: pathname === "/process-data" ? "PROCESS" : "PACKAGING",
          product: 12,
          size: 11,
        },
      },
    }
  );

  const [
    fetchSizesQuery,
    { loading: loadingSizes, data: sizesData, error: sizesError },
  ] = useLazyQuery(PRODUCT_SIZES_QUERY);

  const [
    fetchProductsQuery,
    { loading: loadingProducts, data: productsData, error: productsError },
  ] = useLazyQuery(PRODUCTS_QUERY);

  useEffect(() => {
    // handle product change
    if (!fetchProductsQuery) {
      return;
    }

    fetchProductsQuery({
      variables: {
        where: {
          plant: getCookieByName("plant"),
        },
      },
    });

    // set newly fetched product to the state
    if (productsData && productsData.products) {
      if (productsData.products[0]) {
        setProduct(productsData.products[0].id);
      } else {
        setProduct(null);
      }
      console.log("setting the product");
    }
  }, [fetchProductsQuery, productsData]);

  useEffect(() => {
    // handle size change
    if (!fetchSizesQuery) {
      return;
    }

    if (product) {
      fetchSizesQuery({
        variables: {
          where: {
            product_id: product,
          },
        },
      });
    } else {
      // set size to null if there is no product selected
      setSize(null);
      return;
    }

    if (sizesData && sizesData.sizes) {
      if (sizesData.sizes[0]) {
        setSize(sizesData.sizes[0].id);
      } else {
        setSize(null);
      }
      // console.log(408172, sizesData);
    }
  }, [fetchSizesQuery, sizesData, product]);

  // if there is a size fetch the data
  useEffect(() => {
    if (product === null || size === null) {
      return;
    }

    if (!fetchInspectionData) {
      return;
    }

    if (size) {
      fetchInspectionData({
        variables: {
          where: {
            plant: getCookieByName("plant"),
            type: pathname === "/process-data" ? "PROCESS" : "PACKAGING",
            product: product,
            size: size,
            date_gt: startDate.startOf("day").format(),
            date_lt: endDate.endOf("day").format(),
          },
        },
      });
    }
  }, [size, product, fetchInspectionData, pathname, startDate, endDate]);

  if (loading || loadingProducts || loadingSizes) {
    return <div>Loading</div>;
  }
  if (error || productsError || sizesError) {
    return <div> Something went wrong </div>;
  }

  if (!productsData) {
    return <div> no products were found</div>;
  }

  if (!sizesData) {
    return <div> No Sizes are created yet for the selected size </div>;
  }

  if (!data) {
    return <div> No data received </div>;
  }

  const { inspectionData } = data;

  const { products } = productsData;
  const { sizes } = sizesData;

  return (
    <InspectionDataWrapper>
      <Space direction={"horizontal"}>
        {/* for product */}
        <Form.Item
          label="Select Product"
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
        >
          <Select
            mode="default"
            disabled={loadingProducts}
            name="Product"
            defaultValue={product}
            value={product}
            // showSearch
            // defaultActiveFirstOption={false}

            showArrow
            placeholder="Product"
            filterOption={(input, option) =>
              option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
            }
            // suffixIcon={searchField.loading ? <LoadingOutlined /> : <DownOutlined />}
            onSelect={(value) => {
              setProduct(value);
            }}
          >
            {products.map((product) => {
              return (
                <Option value={product.id} key={product.id}>
                  {product.name}
                </Option>
              );
            })}
          </Select>
        </Form.Item>
        {/* for size */}

        <Form.Item
          label="Select Size"
          labelCol={{ span: 24 }}
          wrapperCol={{ span: 24 }}
        >
          <Select
            mode="default"
            disabled={loadingSizes}
            name="size"
            defaultValue={size}
            value={size}
            // showSearch
            // defaultActiveFirstOption={false}
            showArrow
            placeholder="Size"
            filterOption={false}
            // onSearch={searchField.search}
            // suffixIcon={searchField.loading ? <LoadingOutlined /> : <DownOutlined />}
            onSelect={(value) => {
              setSize(value);
            }}
          >
            {sizes.map((size) => {
              return (
                <Option value={size.id} key={size.id}>
                  {size.size_name}
                </Option>
              );
            })}
          </Select>
        </Form.Item>
      </Space>
      <br />
      <Space direction={"horizontal"}>
        <Form.Item label="Start Date">
          <DatePicker
            allowClear={false}
            onChange={(date, dateString) => {
              if (endDate.diff(date, "days") < 0) {
                showNotifications({
                  type: "error",
                  message: "Please select a date before the end date.",
                });
              } else {
                setStartDate(date);
              }
            }}
            value={startDate}
          />
        </Form.Item>

        {/* for product */}
        <Form.Item label="End Date">
          <DatePicker
            allowClear={false}
            onChange={(date, dateString) => {
              if (startDate.diff(date, "days") > 0) {
                showNotifications({
                  type: "error",
                  message: "Please select a date before the end date.",
                });
              } else {
                setEndDate(date);
              }
            }}
            value={endDate}
          />
        </Form.Item>
      </Space>
      <br />
      {(productsData && productsData.products.length === 0 && (
        <div className="highlight-in-red">This plant has no products yet</div>
      )) || <div>&nbsp;</div>}
      {/* empty code will take an empty line to reserve that space or print the statement. anyway line cost is one */}
      {sizesData && sizesData.sizes.length === 0 && (
        <div className="highlight-in-red">This product has no sizes yet</div>
      )}
      <Table
        rowClassName={(record, index) => {
          if (record.is_deviated === true) {
            return "deviated";
          }
        }}
        loading={loading}
        bordered
        rowKey={(record) => record.id}
        columns={columns({ structure: inspectionData[0], pathname })}
        dataSource={inspectionData}
      />
    </InspectionDataWrapper>
  );
};

export default InspectionData;
