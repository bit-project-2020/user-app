import React from "react";
import { Layout } from "antd";
import styled from "styled-components";
import { withRouter } from "react-router-dom";

import BurgerMenu from "../components/BurgerMenu";

import { getCookieByName } from "../lib/getCookieByName";

const {
  Header,
  Content,
  // Footer
} = Layout;

const AppTitle = styled.h2`
  margin-left: 5.5rem;
  display: inline-block;
  color: #f8f8f8;
  font-weight: bold;
`;

const AppContent = styled.div`
  /* padding: 24px; */
  background: #f8f8f8;
  min-height: 345px;
`;

export const pageNames = {
  // "": `${plant} Dashboard`,
  plants: "Plants",
  products: "Select Product",
  "new-process-record": "New Process Record",
  "new-packaging-record": "New Packaging Record",
  "spec-deviations": "Specification Deviations",
  "process-data": "Process Data",
  "packaging-data": "Packaging Data",
};

// can use if gonna implement a custom burger menu button

// const BurgerMenuButtonWrapper = styled.div`
//   /* height: 20px; */
//   display: flex;
//   flex-direction: column;
//   width: 40px;

//   span {
//     background-color: #f8f8f8;
//     margin-top: 5px;
//     height: 8px;
//     display: block;
//     border-radius: 4px;
//   }
//   #burger-menu-button {
//   }
// `;

// const BurgerMenuButton = () => {
//   return (
//     <BurgerMenuButtonWrapper>
//       <span></span>
//       <span></span>
//       <span></span>
//     </BurgerMenuButtonWrapper>
//   );
// };

const AppLayout = (props) => {
  let plant = getCookieByName("plant");

  if (plant) {
    // TODO uppercase the first letter of the plant
    plant = plant.replace("_", " ");
    // capitalize the first letter
    plant = plant[0].toUpperCase() + plant.substring(1);
  }

  const { children, location } = props;
  const pathname = location.pathname.split("/")[1];

  // header styles
  const headerStyles = {
    background: "#3e92ef",
    padding: 0,
    paddingTop: "5px",
    height: "75px",
  };

  let defaultHeader = true;

  // hide default header for some places
  if (
    [
      "qao-logs",
      "create-process-record",
      "create-packaging-record",
      "create-new-log",
    ].includes(pathname)
  ) {
    defaultHeader = false;
  }

  return (
    <Layout style={{ minHeight: "100vh" }}>
      {/* hide top nav if the page shoudldn't show the default header */}

      <Layout>
        {defaultHeader === true && (
          <Header style={headerStyles}>
            {/* will set the burger menu here */}
            {/* manual burger menu button should come here */}
            {/* <BurgerMenuButton /> */}
            <BurgerMenu />
            <AppTitle>
              {pathname === "" ? `${plant} Dashboard` : pageNames[pathname]}
            </AppTitle>

            {/* <UserMenu
              placement="bottomRight"
              overlay={
                <Menu>
                  <Menu.Item key="logout">
                    <LogoutOutlined /> Sign Out
                  </Menu.Item>
                </Menu>
              }
            >
              <span className="usermenu-text">
                <Avatar
                  shape="square"
                  size={24}
                  className="username-avatar"
                  // src={`https://www.gravatar.com/avatar/${md5('test@haloflights.co.uk')}`}
                />
                <CaretDownOutlined />
              </span>
            </UserMenu> */}
          </Header>
        )}

        <Content>
          <AppContent>{children}</AppContent>
        </Content>
      </Layout>
    </Layout>
  );
};

export default withRouter(AppLayout);
