import React from "react";

import styled from "styled-components";

import User from "../components/User";
import UserCard from "../components/dashboard/UserCard";

const DashboardStyles = styled.div``;

const Profile = () => {
  return (
    <DashboardStyles>
      <User>
        {(data) => {
          if (data) {
            return (
              <>
                <UserCard user={data.currentUser} />
              </>
            );
          }
        }}
      </User>
    </DashboardStyles>
  );
};

export default Profile;
