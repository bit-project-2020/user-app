import React from "react";
import { useParams, useLocation } from "react-router-dom";
import { useQuery } from "@apollo/react-hooks";
import { PRODUCT_QUERY } from "../queries/product";
import { SIZE_QUERY } from "../queries/size";
import { PRODUCT_SPECIFICATIONS_QUERY } from "../queries/productSpecifications";
import { getCookieByName } from "../lib/getCookieByName";
import { YelloHeader } from "../components/YelloHeader";
import InspectionRecordForm from "../components/InspectionRecordForm";
import styled from "styled-components";

const AddNewInspectionRecordWrapper = styled.div`
  background-color: #f0f2f5;
  min-height: 100vh;
  #form-body {
    background-color: #fff;
    padding: 10px;
  }

  .form-product-info {
    margin-bottom: 1rem;
    span {
      font-weight: bold;
    }
  }
`;

const AddNewInspectionRecord = () => {
  const { product_id, size_id } = useParams();
  const { pathname } = useLocation();
  const plant = getCookieByName("plant");

  let isPackaging = false;

  // this is to recognize whether we should render a packaging form or a process form
  if (pathname.indexOf("create-packaging-record") > 0) {
    isPackaging = true;
  }

  // fetch the product data
  const {
    loading: loadingProduct,
    error: productQueryError,
    data: productData,
  } = useQuery(PRODUCT_QUERY, {
    variables: {
      where: {
        id: parseInt(product_id),
      },
    },
  });

  const { loading: sizeLoading, error: sizeError, data: sizeData } = useQuery(
    SIZE_QUERY,
    {
      variables: {
        id: parseInt(size_id),
      },
    }
  );

  const {
    loading: productSpecificationsLoading,
    error: productSpecificationsError,
    data: productSpecificationsData,
  } = useQuery(PRODUCT_SPECIFICATIONS_QUERY, {
    variables: {
      product_id: parseInt(product_id),
      product_size: parseInt(size_id),
      spec_type: isPackaging ? "PACKAGING" : "PROCESS",
    },
  });

  if (loadingProduct || sizeLoading || productSpecificationsLoading) {
    return <div> Loading....! </div>;
  }

  if (productQueryError || sizeError || productSpecificationsError) {
    return <div>something went wrong</div>;
  }

  if (productData && productData.product.plant !== plant) {
    console.log(
      "plant doesn't match so please send the user to the product selection page"
    );
  } else {
    // no need of doing anything
  }

  if (
    productData &&
    //  plantParametersData &&
    sizeData &&
    productSpecificationsData
  ) {
    const sizeName = sizeData.size.size_name;

    return (
      <AddNewInspectionRecordWrapper>
        <YelloHeader isPackaging={isPackaging} />
        <div id="form-body">
          <div className="form-product-info">
            <span> Product: </span> {productData.product.name} &nbsp;{" "}
            <span> Size: </span> {sizeName}
          </div>
          <InspectionRecordForm
            productSpecificationsData={productSpecificationsData}
            isPackaging={isPackaging}
            product_id={product_id}
            size_id={size_id}
          />
        </div>
      </AddNewInspectionRecordWrapper>
    );
  }
};

export default AddNewInspectionRecord;
