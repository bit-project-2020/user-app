import React, { useState } from "react";
import {
  useMutation,
  //useQuery
} from "@apollo/react-hooks";
import {
  useHistory,
  //Link
} from "react-router-dom";

import showNotifications from "../lib/showNotifications";
import { MailOutlined, ArrowLeftOutlined } from "@ant-design/icons";

// import LoadingScreen from "../components/LoadingScreen";

import { REQUEST_PASSWORD_RESET_MUTATION } from "../mutations/requestPasswordReset";
import { RESET_PASSWORD_MUTATION } from "../mutations/ResetPassword";
import { CURRENT_USER_QUERY } from "../queries/currentUser";

import {
  Input,
  Button,
  Card,
  Form as AntForm,
  //Select
} from "antd";
import { Formik } from "formik";
import * as Yup from "yup";
import ChandimaSweetsLogo from "../assets/images/logo.svg";

const FormItem = AntForm.Item;

const RequestResetSchema = Yup.object().shape({
  email: Yup.string().email().required(),
});

const ResetSchema = Yup.object().shape({
  otp: Yup.string().required("Please enter the OTP"),
  password: Yup.string().required("Please enter your password."),
  passwordConfirm: Yup.string().oneOf(
    [Yup.ref("password"), null],
    "Passwords don't match"
  ),
});

const ResetPassword = () => {
  let history = useHistory();
  const [isOtpSent, setOtpSent] = useState(false);

  const [requestReset] = useMutation(REQUEST_PASSWORD_RESET_MUTATION, {
    onCompleted: () => {
      setOtpSent(true);
      showNotifications({ message: "OTP sent to the email address" });
    },
    onError: () => {
      showNotifications({
        type: "error",
        message: "OTP sending Failed!",
      });
    },
  });

  const [resetPassword] = useMutation(RESET_PASSWORD_MUTATION, {
    onCompleted: () => {
      setOtpSent(false);
      // redirect the user to the dashboard
      showNotifications({ message: "Password reset succesful" });
      history.push("/");
    },
    onError: (error) => {
      console.log(error);
      showNotifications({
        type: "error",
        message: "Password reset failed!",
      });
    },
    refetchQueries: [{ query: CURRENT_USER_QUERY }],
  });

  return (
    <div>
      <Card
        style={{
          maxWidth: "350px",
        }}
        className="session-form-card"
        title={
          <div className="logo">
            <img
              id="company-logo"
              src={ChandimaSweetsLogo}
              alt="chandima sweets logo"
            />
            <br />
            🙋‍ Reset Password
          </div>
        }
        actions={[
          <a className="ResetPassword-form-forgot" href="/login">
            Login
          </a>,
        ]}
      >
        <Formik
          initialValues={{
            email: "",
            password: "",
          }}
          validationSchema={isOtpSent ? ResetSchema : RequestResetSchema}
          onSubmit={async (values, { setSubmitting, setErrors }) => {
            // console.log("lets submit");
            if (!isOtpSent) {
              // run sent otp function
              requestReset({
                variables: {
                  email: values.email,
                },
              });
            } else {
              console.log("ready to call reset password");

              // lets try to do the validatin here
              if (values.password !== values.passwordConfirm) {
                // lets try to set the error
                setErrors({
                  passwordConfirm: "password and password confirm didn't match",
                });
                // console.log("passwords doesn't match");
                // showNotifications({
                //   type: "error",
                //   message: "password and password confirm doesn't match",
                // });
                return false;
              }

              resetPassword({
                variables: {
                  otp: values.otp,
                  password: values.password,
                },
              });
              // reset the password with otp, and password resets
            }

            setSubmitting(false);
          }}
        >
          {({
            handleSubmit,
            isSubmitting,
            handleBlur,
            handleChange,
            errors,
            touched,
            values,
          }) => (
            <form onSubmit={handleSubmit}>
              {!isOtpSent && (
                <div>
                  <FormItem
                    validateStatus={
                      touched.email && errors.email ? "error" : "validating"
                    }
                    help={touched.email && errors.email}
                  >
                    <Input
                      name="email"
                      prefix={<MailOutlined />}
                      placeholder="Email"
                      value={values.email}
                      help={errors.email}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                    {/* {errors.email} */}
                  </FormItem>

                  <Button
                    size="small"
                    onClick={() => {
                      setOtpSent(true);
                    }}
                  >
                    I have the OTP
                  </Button>
                  <br />
                  <br />
                </div>
              )}
              {isOtpSent && (
                <>
                  <FormItem
                    validateStatus={
                      touched.otp && errors.otp ? "error" : "validating"
                    }
                    help={touched.otp && errors.otp}
                  >
                    <Input
                      name="otp"
                      type="text"
                      placeholder="OTP"
                      value={values.otp}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </FormItem>
                  <FormItem
                    validateStatus={
                      touched.password && errors.password
                        ? "error"
                        : "validating"
                    }
                    help={touched.password && errors["password"]}
                  >
                    <Input
                      name="password"
                      // prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                      type="password"
                      placeholder="Password"
                      value={values.password}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </FormItem>

                  <FormItem
                    validateStatus={
                      touched.passwordConfirm && errors.passwordConfirm
                        ? "error"
                        : "validating"
                    }
                    help={touched.passwordConfirm && errors["passwordConfirm"]}
                  >
                    <Input
                      name="passwordConfirm"
                      // prefix={<Icon type="lock" style={{ color: 'rgba(0,0,0,.25)' }} />}
                      type="password"
                      placeholder="Confirm Password"
                      value={values["passwordConfirm"]}
                      onChange={handleChange}
                      onBlur={handleBlur}
                    />
                  </FormItem>

                  <Button
                    onClick={() => {
                      setOtpSent(false);
                    }}
                    size="small"
                  >
                    <ArrowLeftOutlined />
                    Go Back
                  </Button>
                </>
              )}

              <FormItem>
                <Button
                  type="primary"
                  htmlType="submit"
                  disabled={isSubmitting}
                  className="session-form-button"
                  loading={isSubmitting}
                >
                  {isOtpSent ? "Reset Password" : "Request Reset"}
                </Button>
              </FormItem>
            </form>
          )}
        </Formik>
      </Card>
    </div>
  );
};

export default ResetPassword;
