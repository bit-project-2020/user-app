import React, {useState, useEffect} from "react";
import CustomHeader from "../components/CustomHeader";
import { SPECIFICATION_DEVIATIONS_QUERY } from "../queries/specificationDeviations";
import { useQuery } from "@apollo/react-hooks";
import { getCookieByName } from "../lib/getCookieByName";
import styled from "styled-components";
import {
  Table,
  Button,
  Space,
  Form,
  DatePicker,
  Input,
  Modal
} from "antd";
import {UserViewSection} from "../components/dashboard/UserLogsSection";
import {TableActions} from "../components/shared/StyledComponent";
import moment from "moment";
import showNotifications from "../lib/showNotifications";
import DeviationDetail from "../components/DeviationDetails";

const { Search } = Input;

const LogsWrapper = styled.div`
  padding: 15px;

  .user-info {
    display: flex;
    img.user-photo-small-round {
      width: 30px;
      height: 30px;
      border-radius: 2rem;
    }
  }

  div.log-info {
    display: flex;
    justify-content: space-around;
  }

  span.shift {
    background-color: #f8f8f8;
    padding: 5px;
  }

  #top {
    padding: 1rem;
    display: flex;
    justify-content: space-between;
    h1 {
      font-weight: bold;
      font-size: 1.2rem;
      margin-bottom: 0px;
    }
    h3 {
      font-size: 0.8rem;
    }
    button {
      color: #3e92ef;
      border-color: #3e92ef;
      border-radius: 5px;
      margin-top: 0.5rem;
    }
  }

  .log-card {
    margin: 0.5rem;
    padding: 0.5rem;
    background-color: #fff;
  }
`;

const columns = (setInitialDeviation) => [
  {
    title: "Deviations",
    dataIndex: "actions",
    align: "center",
    render(txt, deviation, index) {
      const date_and_time = new Date(deviation.created_at);
      return (
        <div className="deviation-card" key={deviation.id} onClick={() => {setInitialDeviation(deviation)}}>
        <div>
          <h4>
            <b>{deviation.product_name}</b>
          </h4>
          <div className="log-info">
            <div className="deviation-section">
              <span>{deviation.parameter} </span>
              <span>{deviation.value}&nbsp;</span>
              <span>{deviation.unit} </span>
            </div>

            <UserViewSection deviation={deviation} />
            <div>
              {date_and_time.toLocaleDateString()} <br />
              {date_and_time.toLocaleTimeString()}
            </div>
          </div>
        </div>
      </div>
      );
    },
  },
];

const SpecialDeviation = () => {
  const [searchValue, setSearchValue] = useState("");
  const [initialDeviation, setInitialDeviation] = useState(null);

  useEffect(() => {
    refetch()
      });

  const { loading, error, data, refetch } = useQuery(SPECIFICATION_DEVIATIONS_QUERY, {
    variables: {
        where: {
            plant: getCookieByName("plant")
          },
    },
  });
  if (error) {
    return <div>error</div>;
  }
  if (loading) {
    return <div> loading</div>;
  }
  if (data) {
    let deviations = data.specificationDeviations;
    // filtered data
    deviations = deviations.filter((deviation) =>
      deviation.product_name.toLowerCase().includes(searchValue.toLowerCase())
    );

    return (
      <div>
        <CustomHeader />
        <LogsWrapper>
          <h1>Special Deviations</h1>

          <div>
            <span>Filtering option section</span>

            <div>
              <h1> Search Results Section</h1>
            </div>
            <TableActions>
          <div className="search-box-wrapper">
            <Search
              value={searchValue}
              className="search-box"
              placeholder="Search by product name"
              onChange={(event) => {
                const { value } = event.target;
                setSearchValue(value);
              }}
            />
          </div>
          </TableActions>
            <Space direction={"horizontal"}>
      </Space>
            <div>
            <Table
          loading={loading}
          bordered
          rowKey={(deviation) => deviation.id}
          columns={columns(setInitialDeviation)}
          dataSource={deviations}
        />
      </div>
          </div>
        </LogsWrapper>
        <Modal
          title="QAO Details"
          visible={initialDeviation !== null}
          onCancel={() => {
            setInitialDeviation(null)
          }}
          onOk={() => {
            console.log("On OK Pressed")
          }}
          footer={null}
        >
          <DeviationDetail deviation={initialDeviation}/>
        </Modal>
      </div>
    );
  }
};

export default SpecialDeviation;