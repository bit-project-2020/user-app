import React from "react";
import { useHistory } from "react-router-dom";

import { YelloHeader } from "../components/YelloHeader";
import styled from "styled-components";
import QAOLogForm from "../components/forms/QAOLogForm";
import { useMutation } from "@apollo/react-hooks";
import { CREATE_QAO_LOG_MUTATION } from "../mutations/createUserLog";
import showNotifications from "../lib/showNotifications";

const CreateLogWrapper = styled.div`
  #log-stage {
    color: red;
    padding: 15px;
  }
`;

const AddNewQAOLog = () => {
  let history = useHistory();
  const [
    createQAOLog,
    // { data, loading, error}
  ] = useMutation(CREATE_QAO_LOG_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Record Created Succesfully!" });
      history.push("/qao-logs");
    },

    onError: (error) => {
      console.log(error);
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
      return false;
    },
  });

  return (
    <CreateLogWrapper>
      <YelloHeader title="New QAO Log" />

      <div id="log-stage">
        <h1>Log details</h1>

        <div id="create-log-form">
          <QAOLogForm
            isUpdate={false}
            createFunction={createQAOLog}
            updateFunction={() => {}}
            initialValues={{}}
          />
        </div>
      </div>
    </CreateLogWrapper>
  );
};

export default AddNewQAOLog;
