import React from "react";
import styled from "styled-components";
import {Button, Space, Image, Table} from 'antd';
import { withRouter } from "react-router-dom";


const InspectionDataOneWrapper = styled.div`
  div#ncr-create-button {
    float: right;
  }

  .center-round{
    border-radius: 50%;
    text-align: center;
    display: block;
    justify-content: center;
    align-items: center;
    margin: auto;
    width: 100%;
  }

  .center{
    text-align: center;
    display: block;
    justify-content: center;
    align-items: center;
    margin: auto;
    width: 100%;
  }

  div.make-bold,
  span.field-title {
    font-weight: bold;
    display: inline-block;
    min-width: 120px;
    text-align: right;
  }
`;

const DeviationDetailContainer = ({
  id,
  value,
  specification,
  created_by,
  first_name,
  last_name,
  profile_photo,
  role,
  product_name,
  unit,
  created_at,
  plant_parameter,
  parameter,
  record_id,
  plant,
  size
}) => {
    return (
      <InspectionDataOneWrapper>
        <br />
        <Space direction={"vertical"}>
          <div color="black" className="center"> 
              <Image width={60} src={profile_photo} className="center-round" />
          </div>
          <div>
            <span className="field-title"> Employee : </span>
            <span> {first_name + " " + last_name} </span>
          </div>
          <div>
            <span className="field-title"> Role : </span>
            <span> {role}</span>
          </div>
          <div>
            <span className="field-title"> Date : </span>
            <span> {created_at}</span>
          </div>

          <div>
            <span className="field-title"> Specification : </span>
            <span> {specification}</span>
          </div>

          <div>
            <span className="field-title"> Product name : </span>
            <span> {product_name} </span>
          </div>
          <div>
            <span className="field-title"> Plant : </span>
            <span> {plant}</span>
          </div>
          <div>
            <span className="field-title"> Unit : </span>
            <span> {unit}</span>
          </div>
        </Space>
      </InspectionDataOneWrapper>
    );
};

export default withRouter(DeviationDetailContainer);
