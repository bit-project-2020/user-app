import React from "react";
import {
  BrowserRouter as ReactRouter,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";

import AppLayout from "../containers/AppLayout";
import Dashboard from "../containers/Dashboard";
import InspectionRecord from "../containers/InspectionDataOne";

import NewInspectionRecord from "../containers/AddNewInspectionRecord";
import PlantProductCards from "../containers/PlantProductCards";

import QAOLogs from "../containers/QAOLogs";
import AddNewQAOLog from "../containers/AddNewQAOLog";

import Specifications from "../containers/Specifications";
import Profile from "../containers/Profile";

import InspectionData from "../containers/InspectionData";
import SpecialDeviation from '../containers/SpecialDeviation';

const Router = () => (
  <ReactRouter>
    <div>
      <AppLayout>
        <Switch>
          <Route path="/" exact component={Dashboard} />

          <Route
            path="/specifications/:product_id/:size_id"
            exact
            component={Specifications}
          />

          <Route
            path="/new-process-record"
            exact
            component={PlantProductCards}
          />
          <Route
            path="/new-packaging-record"
            exact
            component={PlantProductCards}
          />

          <Route path="/products" exact component={PlantProductCards} />

          <Route path="/qao-logs" exact component={QAOLogs} />
          <Route path="/spec-deviations" exact component={SpecialDeviation} />
          <Route path="/create-new-log" exact component={AddNewQAOLog} />

          <Route
            path="/create-packaging-record/:product_id/:size_id"
            exact
            component={NewInspectionRecord}
          />
          <Route
            path="/create-process-record/:product_id/:size_id"
            exact
            component={NewInspectionRecord}
          />
          <Route path="/profile" exact component={Profile} />

          <Route path="/process-data" exact component={InspectionData} />
          <Route path="/packaging-data" exact component={InspectionData} />

          <Route
            path="/:path_name/:plant/:record_id"
            exact
            component={InspectionRecord}
          />

          <Redirect to="/" from="/login" push />
          <Redirect to="/" from="/forgotpassword" push />
        </Switch>
      </AppLayout>
    </div>
  </ReactRouter>
);

export default Router;
