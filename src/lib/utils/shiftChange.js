export const shiftChanger = (timeFromInput) => {
  if (timeFromInput > 6 && timeFromInput < 14) {
    return "shift-1";
  } else if (timeFromInput > 14 && timeFromInput < 22) {
    return "shift-2";
  } else {
    return "shift-3";
  }
};
