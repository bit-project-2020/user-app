import { gql } from "apollo-boost";

export const PRODUCT_SIZES_QUERY = gql`
  query SizesQuery($where: SizeWhereInput!) {
    sizes(where: $where) {
      id
      product
      size_name
      is_active
    }
  }
`;
