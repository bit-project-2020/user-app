import { gql } from "apollo-boost";

const INSPECTION_RECORD_ONE_QUERY = gql`
  query inspection_data_record_query(
    $record_type: PARAMETER_MODE!
    $plant: String!
    $record_id: Int!
  ) {
    inspectionDataRecord(
      record_type: $record_type
      plant: $plant
      recordId: $record_id
    ) {
      id
      date
      created_by
      approved_by
      product {
        id
        plant
        name
        is_active
        created_at
      }
      size {
        id
        size_name
      }
      shift
      extra_fields {
        type
        specification
        boolean_value
        numeric_value
        text_value
        parameter
      }
      deviations {
        id
        value
        size
        specification
        parameter
        plant_parameter
      }
      ncr {
        id
        created_by
        plant
        qa_representative
        deviated_parameters
        reason
        production_representative
        created_at
        is_active
        comment
        record_id
      }
    }
  }
`;

export { INSPECTION_RECORD_ONE_QUERY };
