import { gql } from "apollo-boost";

export const PRODUCT_QUERY = gql`
  query product($where: ProductWhereUniqueInput!) {
    product(where: $where) {
      id
      plant
      name
      is_active
    }
  }
`;
