import { gql } from "apollo-boost";

const USER_QUERY = gql`
  query user_query($where: UserWhereUniqueInput!) {
    user(where: $where) {
      id
      first_name
      last_name
      role
      emp_no
      email
      is_active
      profile_photo
    }
  }
`;

export default USER_QUERY;
