import { gql } from "apollo-boost";

export const SIZE_QUERY = gql`
  query size_query($id: Int!) {
    size(where: { id: $id }) {
      id
      size_name
    }
  }
`;
