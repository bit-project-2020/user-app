import { gql } from 'apollo-boost';

export const PLANTS_QUERY = gql`
  query PlantQuery($where:PlantWhereUniqueInput){
    plants(where:$where){
      plant_name
      is_active
    }
  }
`;