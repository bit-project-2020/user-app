import { gql } from "apollo-boost";

export const PRODUCTS_QUERY = gql`
  query productsQuery($where: ProductWhereInput!) {
    products(where: $where) {
      id
      name
      is_active
      image_url
    }
  }
`;
