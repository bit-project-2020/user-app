import {gql} from 'apollo-boost';

export const SPEC_PARAMETERS_QUERY = gql`
  query specParameters {
    specParameters {
      name
      parameter_type
    }
  }
`;