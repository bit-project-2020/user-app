import { gql } from "apollo-boost";

const CURRENT_USER_QUERY = gql`
  query {
    currentUser {
      id
      first_name
      last_name
      profile_photo
      email
      role
      last_name
    }
  }
`;

export { CURRENT_USER_QUERY };
