import { gql } from "apollo-boost";

export const SPECIFICATION_DEVIATIONS_QUERY = gql`
  query specification_deviations_query(
    $where: SpecificationDeviationsWhereInput
  ) {
    specificationDeviations(where: $where) {
      id
      value
      specification
      created_by
      first_name
      last_name
      profile_photo
      role
      product_name
      unit
      created_at
      plant_parameter
      parameter
      record_id
      plant
      size
      __typename
    }
  }
`;
