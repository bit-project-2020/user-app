import { gql } from "apollo-boost";

export const PRODUCT_SPECIFICATIONS_QUERY = gql`
  query product_specifications_query(
    $product_id: Int!
    $product_size: Int!
    $spec_type: PARAMETER_MODE!
  ) {
    productSpecifications(
      where: {
        product_id: $product_id
        product_size: $product_size
        spec_type: $spec_type
      }
    ) {
      size {
        id
        size_name
      }
      product {
        id
        plant
        name
        is_active
        image_url
      }
      plant_param_with_spec {
        parameter
        parameter_type
        specification
        unit
        is_active
      }
    }
  }
`;
