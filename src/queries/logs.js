import { gql } from "apollo-boost"

export const LOGS_QUERY = gql`
  query logs_query($plant: String!, $start: Date, $end: Date) {
    logs(where: { plant: $plant, created_at_gt: $start, created_at_lt: $end}) {
      id
      plant
      created_at
      created_by
      title
      shift
      log
      first_name
      last_name
      profile_photo
      role
    }
  }
`
