import React from "react";

export const ErrorVisualizer = ({ spec, parameter_type, value }) => {
  if (parameter_type === "NUMBER" && spec != null && value != null) {
    const [min, max] = spec.split(",");

    if (value < min) {
      return <div> The safe value should be greater than {min} </div>;
    } else if (value > max) {
      return <div> The safe value should be less than {max} </div>;
    }

    return <div></div>;
  } else if (parameter_type === "NUMBER_EXACT") {
    if (spec != null) {
      const expectedValue = parseFloat(spec);
      if (value - expectedValue > 0.01) {
        return <div> The safe value is ${spec} </div>;
      }
    }
  } else if (parameter_type === "BOOLEAN") {
    if (value != null && spec != null) {
      const expectedValue = spec === "true" ? true : false;
      if (value !== expectedValue) {
        return (
          <div> The safe value is {expectedValue ? '"Ok"' : '"Not Ok"'} </div>
        );
      }
    }
  }
  return <div></div>;
};
