/* eslint-disable jsx-a11y/anchor-is-valid */
/* eslint-disable jsx-a11y/alt-text */
import styled from "styled-components";
import React, { useState } from "react";
import { Link } from "react-router-dom";
import { slide as BurgerMenuSlider } from "react-burger-menu";
import { useMutation } from "@apollo/react-hooks";

import { CURRENT_USER_QUERY } from "../components/User";
import showNotifications from "../lib/showNotifications";
import { LOGOUT_MUTATION } from "../mutations/logout";

import Dashboard_icon from "../assets/icons/dashboard.svg";
import Products_icon from "../assets/icons/product.svg";
import Data_icon from "../assets/icons/data_file.svg";
import Circuit_icon from "../assets/icons/testing.svg";
import User_icon from "../assets/icons/user.svg";

import Products_icon_black from "../assets/icons/product_black.svg";
import Data_icon_black from "../assets/icons/data_file_black.svg";
import Circuit_icon_black from "../assets/icons/testing_black.svg";
import User_icon_black from "../assets/icons/user_black.svg";

import { useLocation } from "react-router-dom";

const styles = {
  bmBurgerButton: {
    position: "fixed",
    width: "36px",
    height: "30px",
    marginTop: "1rem",
    marginLeft: "2rem",

    // left: "36px",
    // top: "15px",
    // marginRight: '2rem'
  },

  bmBurgerBars: {
    background: "#f8f8f8",
  },
  bmCrossButton: {
    height: "24px",
    width: "24px",
    right: "18px",
    top: "12px",
    borderRadius: "50%",
    border: "16px solid #ffffff",
  },
  bmCross: {
    background: "#3174be",
    top: "-13px",
    left: "13px",
  },
  bmMenuWrap: {
    top: "0",
    background: "#ffffff",
    pointerEvents: "none",
  },
  bmMenu: {
    background: "white",
    padding: "2.5em 1.5em 0",
    fontSize: "1.15em",
    borderLeft: "px solid #262262",
    opacity: "0.97",
    pointerEvents: "none",
  },
  bmMorphShape: {
    fill: "#373a47",
  },
  bmItemList: {
    color: "#b8b7ad",
    padding: "0.8em",
    pointerEvents: "auto",
  },
  bmItem: {
    // display: "block",
    display: "flex",
    alignItems: "baseline",
    color: "#777777",
    fontSize: "1.3rem",
    lineHeight: "2rem",
    marginTop: "0.2rem",
    // textTransform: 'uppercase'
    fontWeight: "bold",
    marginBottom: "1.2rem",
  },
  bmOverlay: {
    background: "rgba(0, 0, 0, 0)",
  },
  logout_wrap: {
    marginTop: "2.2rem",
  },
  // check app.css for react-burger-menu-btn class overridings
};

const BurgerMenuWrapper = styled.div`
  img.menu {
    filter: opacity(0.5);
  }

  img.icon {
    // filter: opacity(0.5);
    margin-right: 1rem;
    height: 1.2rem;
    padding-top: 2px;
  }

  img.icon.black {
    display: none;
  }

  span.anticon.anticon-group {
    font-size: 1.5rem;
    margin-right: 1rem;
  }

  .bm-item.active .lbl {
    color: #000000;
  }

  .bm-item.active img.menu {
    filter: opacity(1);
    display: inline !important;
  }

  .bm-item.active img.icon {
    display: none;
  }

  .bm-item.active img.icon.black {
    display: inline;
  }

  #logout-button {
    width: 130%;
    background-color: #7a849c;
    margin: 1rem -2.3rem;
    text-align: center;
    overflow: hidden;
    padding: 0.5rem;
    margin-top: 2.2rem !important;
    justify-content: center;
    button {
      color: #ffffff;
      background: transparent;
      border: none;
      cursor: pointer;
    }
  }

  span#copyright-text.bm-item {
    position: absolute;
    bottom: 0;
    font-size: 1rem !important;
    opacity: 0.6;
    color: #8d8d8d;
  }

  span.lbl.active {
    color: #000000 !important;
  }
`;

const BurgerMenu = () => {
  let location = useLocation();

  //console.log(654123, location);
  const [menuOpen, setMenuOpen] = useState(false);
  //console.log(6545, menuOpen);

  const [logout] = useMutation(LOGOUT_MUTATION, {
    onCompleted: () => {
      // console.log(222, "on completed called");
      showNotifications({ message: "Logout Succesful!" });
    },
    onError: () => {
      // console.log(111, "onError called");
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
    },
    refetchQueries: [{ query: CURRENT_USER_QUERY }],
  });

  return (
    <BurgerMenuWrapper>
      <BurgerMenuSlider isOpen={menuOpen} styles={styles}>
        <a
          id="home"
          href="/"
          className={`menu-item ${
            location.pathname === "/" ? "active" : "inactive"
          }`}
        >
          {/* <GroupOutlined /> */}
          <img src={Dashboard_icon} className="icon menu" />
          <span className="lbl">Dashboard</span>
        </a>
        <a
          id="home"
          href="/products?viewspecs=1"
          className={`menu-item ${
            location.pathname === "/products" ? "active" : "inactive"
          }`}
        >
          <img src={Products_icon} className="icon" />
          <img src={Products_icon_black} className="icon black" />
          <span className="lbl">Products</span>
        </a>
        {/* <Link
          onClick={async () => {
            await setMenuOpen(false);
          }}
          to="/new-process-record"
          className={`menu-item ${
            location.pathname === "/new-process-record" ? "active" : "inactive"
          }`}
        >
          <img src={Data_icon} className="icon" />
          <img src={Data_icon_black} className="icon black" />
          <span className="lbl">New Process Data</span>
        </Link> */}
        <a
          onClick={async () => {
            await setMenuOpen(false);
          }}
          href="/new-process-record"
          className={`menu-item ${
            location.pathname === "/new-process-record" ? "active" : "inactive"
          }`}
        >
          <img src={Data_icon} className="icon" />
          <img src={Data_icon_black} className="icon black" />
          <span className="lbl">New Process Data</span>
        </a>
        {/* <Link
          to="/new-packaging-record"
          className={`menu-item ${
            location.pathname === "/new-packaging-record"
              ? "active"
              : "inactive"
          }`}
        >
          <img src={Data_icon} className="icon" />
          <img src={Data_icon_black} className="icon black" />
          <span className="lbl">New Packaging Data</span>
        </Link> */}
        <a
          href="/new-packaging-record"
          className={`menu-item ${
            location.pathname === "/new-packaging-record"
              ? "active"
              : "inactive"
          }`}
        >
          <img src={Data_icon} className="icon" />
          <img src={Data_icon_black} className="icon black" />
          <span className="lbl">New Packaging Data</span>
        </a>

        <Link
          to="/qao-logs"
          className={`menu-item ${
            location.pathname === "/qao-logs" ? "active" : "inactive"
          }`}
        >
          <img src={Circuit_icon} className="icon" />
          <img src={Circuit_icon_black} className="icon black" />
          <span className="lbl">QAO logs</span>
        </Link>

        <Link
          to="/process-data"
          className={`menu-item ${
            location.pathname === "/process-data" ? "active" : "inactive"
          }`}
        >
          <img src={Circuit_icon} className="icon" />
          <img src={Circuit_icon_black} className="icon black" />
          <span className="lbl">Process Data</span>
        </Link>

        <Link
          to="/packaging-data"
          className={`menu-item ${
            location.pathname === "/packaging-data" ? "active" : "inactive"
          }`}
        >
          <img src={Circuit_icon} className="icon" />
          <img src={Circuit_icon_black} className="icon black" />
          <span className="lbl">Packaging Data</span>
        </Link>

        <Link
          to="/spec-deviations"
          className={`menu-item ${
            location.pathname === "/spec-deviations" ? "active" : "inactive"
          }`}
        >
          <img src={Circuit_icon} className="icon" />
          <img src={Circuit_icon_black} className="icon black" />
          <span className="lbl">Spec. Deviations</span>
        </Link>
        <Link
          to="/profile"
          className={`menu-item ${
            location.pathname === "/profile" ? "active" : "inactive"
          }`}
        >
          <img src={User_icon} className="icon" />
          <img src={User_icon_black} className="icon black" />
          <span className="lbl">Profile</span>
        </Link>
        <span id="logout-button" className="logout_wrap">
          <button
            onClick={() => {
              logout();
            }}
          >
            LOGOUT
          </button>
        </span>

        <span id="copyright-text"> Chandima Sweets ©2020</span>
      </BurgerMenuSlider>
    </BurgerMenuWrapper>
  );
};

export default BurgerMenu;
