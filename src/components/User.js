import React from "react";
import { useQuery } from "@apollo/react-hooks";
import { gql } from "apollo-boost";
import LoadingScreen from "./LoadingScreen";

import avatarImage from "../assets/images/avatar.jpg";

export const CURRENT_USER_QUERY = gql`
  query {
    currentUser {
      id
      first_name
      last_name
      profile_photo
      email
      role
      last_name
    }
  }
`;

const User = (props) => {
  const {
    loading,
    // error,
    data,
  } = useQuery(CURRENT_USER_QUERY);

  if (loading) return <LoadingScreen />;
  if (data) {
    // if user has no image added when creating place an avatar there
    if (data.currentUser && data.currentUser.profile_photo === null) {
      data.currentUser.profile_photo = avatarImage;
    }

    return <> {props.children(data)} </>;
  }
};

export default User;
