/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { ArrowRightOutlined } from "@ant-design/icons";
import { Button } from "antd";
import { useQuery } from "@apollo/react-hooks";
import styled from "styled-components";

import { useHistory } from "react-router-dom";

import { LOGS_QUERY } from "../../queries/logs";
import { getCookieByName } from "../../lib/getCookieByName";

const roleAcronym = {
  SYSTEM_ADMIN: "ADMIN",
  QA_OFFICER: "QAO",
  QA_EXECUTIVE: "QAE",
  QA_MANAGER: "QAM",
  PRODUCTION_MANAGER: "Prod. Mgr.",
  PRODUCTION_OFFICER: "Prod. Officer",
};

const UserLogsWrapper = styled.div`
  .user-info {
    display: flex;
    img.user-photo-small-round {
      width: 30px;
      height: 30px;
      border-radius: 2rem;
    }
  }

  div.log-info {
    display: flex;
    justify-content: space-around;
  }

  span.shift {
    background-color: #f8f8f8;
    padding: 5px;
  }

  #top {
    padding: 1rem;
    display: flex;
    justify-content: space-between;
    h1 {
      font-weight: bold;
      font-size: 1.2rem;
      margin-bottom: 0px;
    }
    h3 {
      font-size: 0.8rem;
    }
    button {
      color: #3e92ef;
      border-color: #3e92ef;
      border-radius: 5px;
      margin-top: 0.5rem;
    }
  }
  .log-card {
    margin: 0.5rem;
    padding: 0.5rem;
    background-color: #fff;
  }
`;

const UserLogsSection = () => {
  let history = useHistory();
  const { loading, error, data } = useQuery(LOGS_QUERY, {
    variables: {
      plant: getCookieByName("plant"),
    },
  });
  // fetch most recent two qa logs
  if (loading) {
    return <div>Loading !</div>;
  }
  if (error) {
    return <div>Something went wrong !</div>;
  }

  if (data) {
    console.log(546512, data);
    const { logs } = data;
    return (
      <UserLogsWrapper>
        <div id="top">
          <div>
            <h1> QAO Logs </h1>
            <h3> Most recent 2 logs</h3>
          </div>

          <Button
            onClick={() => {
              history.push("/qao-logs");
            }}
          >
            see more <ArrowRightOutlined />
          </Button>
        </div>
        <div id="log-cards">
          {logs.map((log) => {
            const date_and_time = new Date(log.created_at);
            console.log(log);
            return (
              <div className="log-card" key={log.id}>
                <div>
                  <h4>
                    <b> {log.title}</b>
                  </h4>
                  <div className="log-info">
                    <span className="shift"> {log.shift} </span>
                    <UserViewSection log={log} />
                    <div>
                      {date_and_time.toLocaleDateString()}
                      {date_and_time.toLocaleTimeString()}
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </UserLogsWrapper>
    );
  }
};

export default UserLogsSection;

const UserViewWrapper = styled.div`
  display: flex;
  img.user-photo-small-round {
    width: 30px;
    height: 30px;
    border-radius: 2rem;
  }

  .make-center {
    display: flex;
    justify-content: center;
  }
`;

export const UserViewSection = ({ deviation, log }) => {
  deviation = deviation || log;
  return (
    <UserViewWrapper>
      <img
        className="user-photo-small-round"
        src={deviation.profile_photo}
      ></img>
      <div>
        <div className="make-center">
          <span>
            {deviation.first_name[0].toUpperCase() +
              deviation.first_name.substring(1)}
          </span>
          {/* <span> // removing last name in order to save the space
              {deviation.last_name[0].toUpperCase() +
                deviation.last_name.substring(1)}
            </span> */}
        </div>
        <span> ({roleAcronym[deviation.role]}) </span>
      </div>
    </UserViewWrapper>
  );
};
