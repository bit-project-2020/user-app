import React from "react";
import styled from "styled-components";
import capitalizeInitial from "../../lib/capitalizeInitial";

const UserCardWrapper = styled.div`
  display: flex;
  background-color: #fff;
  margin-top: 10px;
  padding-top: 0.5rem;
  padding-bottom: 0.5rem;
  padding-left: 1.5rem;

  img {
    max-width: 109px;
    max-height: 86px;
    border-radius: 4rem;
    border-color: black;
    border-width: 2px;
    border-style: solid;
  }

  #card-content {
    margin-left: 1rem;

    h2 {
      margin-bottom: 0.3rem;
      font-size: 1.5rem;
    }
    h3 {
      margin-bottom: 0rem;
      color: #d79836;
    }
  }
`;

const UserCard = ({ user }) => {
  return (
    <UserCardWrapper>
      <img src={user.profile_photo} alt="profile"></img>
      <div id="card-content">
        <h2>
          {capitalizeInitial(user.first_name)} &nbsp;
          {capitalizeInitial(user.last_name)}
        </h2>
        <span>
          <h3> {user.email}</h3>
          <h3> {user.role} </h3>
        </span>
      </div>
    </UserCardWrapper>
  );
};

export default UserCard;
