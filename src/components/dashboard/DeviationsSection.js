/* eslint-disable jsx-a11y/alt-text */
import React from "react";
import { ArrowRightOutlined } from "@ant-design/icons";
import { Button } from "antd";
import { useQuery } from "@apollo/react-hooks";
import styled from "styled-components";
import moment from "moment";

import { useHistory } from "react-router-dom";

import { SPECIFICATION_DEVIATIONS_QUERY } from "../../queries/specificationDeviations";
import { getCookieByName } from "../../lib/getCookieByName";
import { UserViewSection } from "../../components/dashboard/UserLogsSection";

const DeviationsWrapper = styled.div`
  div.log-info {
    display: flex;
    justify-content: space-around;
    align-items: center;

    .deviation-section {
      background: #ec5e99;
      padding: 5px;
      color: #fff;
      font-weight: bold;
      border-radius: 4px;
      height: 32px;
      min-width: 130px;
      text-align: center;
    }
  }

  #top {
    padding: 1rem;
    display: flex;
    justify-content: space-between;
    h1 {
      font-weight: bold;
      font-size: 1.2rem;
      margin-bottom: 0px;
    }
    h3 {
      font-size: 0.8rem;
    }
    button {
      color: #3e92ef;
      border-color: #3e92ef;
      border-radius: 5px;
      margin-top: 0.5rem;
    }
  }
  .deviation-card {
    margin: 0.5rem;
    padding: 0.5rem;
    background-color: #fff;
  }
`;

const DeviationsSection = () => {
  let history = useHistory();
  const { loading, error, data } = useQuery(SPECIFICATION_DEVIATIONS_QUERY, {
    variables: {
      where: {
        plant: getCookieByName("plant"),
      },
    },
  });
  // fetch most recent two qa logs
  if (loading) {
    return <div>Loading !</div>;
  }
  if (error) {
    console.log(error);
    return <div>Something went wrong !</div>;
  }

  if (data) {
    // console.log(66666, data);
    const { specificationDeviations } = data;
    return (
      <DeviationsWrapper>
        <div id="top">
          <div>
            <h1> Specification Deviations </h1>
            <h3> Most recent deviations </h3>
          </div>

          {/* // lets hide this button temporally */}
          {/* <Button
            onClick={() => {
              history.push("/qao-logs");
            }}
          >
            see more <ArrowRightOutlined />
          </Button> */}
        </div>
        <div id="log-cards">
          {specificationDeviations.slice(0, 5).map((deviation) => {
            console.log(100100, deviation.created_at);
            const date_and_time = new Date(deviation.created_at);
            console.log(6000200, date_and_time);

            // have to address binary specfications
            if (deviation.value === "true" || deviation.value === "false") {
              console.log(8888, deviation.value);
              // alter the deviation value
              deviation.value = deviation.value === "true" ? "Ok" : "Not Ok";
            }
            return (
              <div className="deviation-card" key={deviation.id}>
                <div>
                  <h4>
                    <b>{deviation.product_name}</b>
                  </h4>
                  <div className="log-info">
                    <div className="deviation-section">
                      <span>{deviation.parameter} </span>
                      <span>{deviation.value}&nbsp;</span>
                      <span>{deviation.unit != "N/A" && deviation.unit} </span>
                    </div>

                    <UserViewSection deviation={deviation} />
                    <div>
                      {date_and_time.toLocaleDateString()} <br />
                      {date_and_time.toLocaleTimeString()}
                    </div>
                  </div>
                </div>
              </div>
            );
          })}
        </div>
      </DeviationsWrapper>
    );
  }
};

export default DeviationsSection;
