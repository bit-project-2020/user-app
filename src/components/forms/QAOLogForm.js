import React from "react"; // , { useState }
import { Formik } from "formik";
import { getCookieByName } from "../../lib/getCookieByName";

import { DefaultEditor } from "react-simple-wysiwyg";

import {
  Button,
  Form as AntForm,
  // Select,
  Input,
} from "antd";

const FormItem = AntForm.Item;

// const { Option } = Select;

const QAOLogForm = ({
  isUpdate,
  initialValues,
  updateFunction,
  createFunction,
}) => {
  initialValues.log = "my <b>HTML</b>";
  // const [files, setFiles] = useState(initialValues.profile_photo);
  // const [log, setLog] = React.useState("my <b>HTML</b>");

  return (
    <Formik
      validateOnBlur={false}
      validateOnChange={false}
      initialValues={{ ...initialValues }}
      // validationSchema={schema.validationSchema}
      onSubmit={async (values, { setSubmitting }) => {
        try {
          const updatedValues = { ...values };

          setSubmitting(true);

          // console.log(
          //   77889955,
          //   values["profile_photo"],
          //   values["profile_photo"][0].status,
          //   values.profile_photo[0].originFileObj
          // );

          if (isUpdate) {
            // delete conflicting fields
            delete updatedValues.id;
            delete updatedValues.__typename;

            // updateFunction({
            //   variables: {
            //     where: {
            //       id: parseInt(initialValues.id),
            //     },
            //     data: {
            //       ...updatedValues,
            //     },
            //   },
            // });
          } else {
            createFunction({
              variables: {
                plant: getCookieByName("plant"),
                ...values,
              },
            });
            // run create function
          }

          setSubmitting(false);
        } catch (e) {
          console.log(e);
        }
      }}
    >
      {({
        handleSubmit,
        isSubmitting,
        handleBlur,
        handleChange,
        errors,
        values,
        setFieldValue,
      }) => {
        return (
          <form onSubmit={handleSubmit}>
            <FormItem
              key="log_title"
              // label="log_title"
              // validateStatus={errors[formItem.name] ? 'error' : 'validating'}
              // help={errors[formItem.name]}
              required={true} // this will show that the fields are required
            >
              <Input
                value={values.title}
                onChange={handleChange}
                name="title"
                placeholder="Log Title"
                required={true}
              />
            </FormItem>

            {/* <FormItem
              key="datentime"
              // label="log_title"
              // validateStatus={errors[formItem.name] ? 'error' : 'validating'}
              // help={errors[formItem.name]}
              required={true} // this will show that the fields are required
            >
              <input
                type="datetime-local"
                name="datentime"
                onChange={(e) => {
                  // shiftChanger(e.target.value);
                  // handleChange(e);
                  // setFieldValue("shift", e.target.value);
                }}
                onBlur={handleBlur}
                value={values["datentime"]}
                placeholder={"Date"}
              />
            </FormItem> */}

            {/* <FormItem
              key="shift"
              // label="last_name"
              // validateStatus={errors[formItem.name] ? 'error' : 'validating'}
              // help={errors[formItem.name]}
              required={true}
            >
              <Select
                name="shift"
                value={values["shift"]}
                // defaultValue="shift-1"
                style={{ width: 120 }}
                // onChange={handleChange}
                onSelect={(value) => {
                  setFieldValue("shift", value);
                }}
              >
                <Option value="shift-1">Shift 1</Option>
                <Option value="shift-2">Shift 2</Option>
                <Option value="shift-3">Shift 3</Option>
              </Select>
            </FormItem> */}

            <FormItem
              key="log"
              // label="log"
              // validateStatus={errors[formItem.name] ? 'error' : 'validating'}
              // help={errors[formItem.name]}
              required={true}
            >
              <DefaultEditor
                value={values["log"]}
                onChange={(e) => {
                  setFieldValue("log", e.target.value);
                }}
              />
            </FormItem>
            <FormItem
              wrapperCol={{
                xs: { span: 24, offset: 0 },
                sm: { span: 6, offset: 3 },
                xl: { span: 6, offset: 2 },
              }}
            >
              <Button
                type="primary"
                htmlType="submit"
                disabled={isSubmitting}
                className="session-form-button"
                loading={isSubmitting}
                //icon={<PlusOutlined />}
              >
                {isUpdate ? "Update" : "Create"} Log
              </Button>
            </FormItem>
          </form>
        );
      }}
    </Formik>
  );
};

export default QAOLogForm;
