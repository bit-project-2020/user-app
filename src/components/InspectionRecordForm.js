import React from "react";
import { Formik } from "formik";
import styled from "styled-components";
import moment from "moment";
import ImgCrop from "antd-img-crop";
import uploadFile from "../lib/uploadFile";
import { useHistory } from "react-router-dom";

import {
  Switch,
  Input,
  Button,
  InputNumber,
  Space,
  DatePicker,
  TimePicker,
  Form as AntForm,
  Upload,
  Modal,
} from "antd";

import { CloseOutlined } from "@ant-design/icons";

import { useMutation } from "@apollo/react-hooks";
import { CREATE_NEW_INSPECTION_RECORD_MUTATION } from "../mutations/createNewInspectionRecord";
import { ErrorVisualizer } from "../components/ErrorVisualizer";

import { shiftChanger } from "../lib/utils/shiftChange";
import showNotifications from "../lib/showNotifications";

const { confirm } = Modal;

const FormItem = AntForm.Item;

const { TextArea } = Input;

const FormikStypeWrapper = styled.div`
  /* .ant-input-number {
    margin-top: 1.1rem;
  } */

  input[type="datetime-local"] {
    border-color: #d9d9d9;
    border-radius: 3.5px;
    padding: 0.2rem;
  }

  label {
    text-align: right;
    width: 120px;
    display: inline-block;
    margin-right: 5px;
    font-weight: bold;
  }

  .ant-select.ant-select-single.ant-select-show-arrow.ant-select-show-search,
  .ant-select.ant-select-single.ant-select-show-arrow {
    margin-right: 0.5rem;
    margin-top: 0.5rem;
  }

  .error-visualizer-box {
    min-height: 20px;
    color: red;
    text-align: center;
  }

  .ant-form-item {
    margin-bottom: 0px;
  }

  .date-time-section {
    margin-bottom: 1rem;
  }

  button.ant-btn.ant-btn-primary {
    margin-left: 3rem;
  }

  .whitespace-no-wrap {
    white-space: nowrap;
  }

  .active {
    color: #1890ff;
    min-width: 50px;
    display: inline-block;
  }
  .inactive {
    color: rgba(0, 0, 0, 0.25);
    min-width: 50px;
    display: inline-block;
  }
`;

// const { Option } = Select;
// define createProcessInspectionRecord mutation here

// const FieldDuet = styled.div`
//   display: flex;
//   color: red;
// `;

export default function InspectionRecordForm({
  productSpecificationsData,
  isPackaging,
  // plantPramesWithSpecs,
  product_id,
  size_id,
}) {
  let history = useHistory();
  const initialValues = {
    currentDate: moment(),
    currentTime: moment(),
  };
  const shift = shiftChanger(moment().hour());

  initialValues.shift = shift;

  let {
    plant_param_with_spec,
    product,
  } = productSpecificationsData.productSpecifications;

  const [
    createProcessInspectionRecord,
    // { data, loading, error}
  ] = useMutation(CREATE_NEW_INSPECTION_RECORD_MUTATION, {
    onCompleted: () => {
      showNotifications({ message: "Record Created Succesfully!" });
      //  temporally commenting to enter many data
      // history.push("/");
    },

    onError: (error) => {
      console.log(error);
      showNotifications({
        type: "error",
        message: "Something went wrong! Please try again.",
      });
      return false;
    },
  });

  // setting initial values new way

  plant_param_with_spec.forEach((param) => {
    // set true as the initial value of all boolean parameters
    // if (param.parameter_type === "BOOLEAN") {
    //   initialValues[param.parameter] = true;
    // } else {
    //   initialValues[param.parameter] = null;
    // }
    initialValues[param.parameter] = null;
  });

  // console.log(120, plant_param_with_spec);

  // filter and get only is_active true parameters
  plant_param_with_spec = plant_param_with_spec.filter(
    (parameter) => parameter.is_active === true
  );

  // get all the measurement params
  const booleanParams = plant_param_with_spec.filter(
    (parameter) => parameter.parameter_type === "BOOLEAN"
  );

  const numericParams = plant_param_with_spec.filter(
    (parameter) =>
      parameter.parameter_type === "NUMBER" ||
      parameter.parameter_type === "NUMBER_EXACT"
  );

  const textParams = plant_param_with_spec.filter(
    (parameter) => parameter.parameter_type === "TEXT"
  );

  const dateParams = plant_param_with_spec.filter(
    (parameter) => parameter.parameter_type === "DATE"
  );

  const imageParams = plant_param_with_spec.filter(
    (parameter) => parameter.parameter_type === "IMAGE"
  );

  // console.log(798656, initialValues);
  // set current date to the datentime object

  return (
    <FormikStypeWrapper>
      <Formik
        enableReinitialize={true}
        initialValues={initialValues}
        validate={(values) => {
          // const errors = {};
          // handle validations here
          // handle email errors
          //  if (!values.email) {
          //    errors.email = 'Required';
          //  } else if (
          //    !/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)
          //  ) {
          //    errors.email = 'Invalid email address';
          //  }
          //  return errors;
        }}
        onSubmit={async (values, { setSubmitting }) => {
          try {
            setSubmitting(true);
            const updatedValues = {};
            updatedValues.plant = product.plant;
            updatedValues.type = isPackaging ? "PACKAGING" : "PROCESS";

            // create a new Date from given date time.
            updatedValues.extra_fields = [];
            updatedValues.product = parseInt(product_id);
            updatedValues.size = parseInt(size_id);
            updatedValues.shift = values.shift;
            updatedValues.comment = values.comment;

            // lets get the date and time from two pickers
            const formattedDate = values.currentDate.format("MM-DD-YYYY");
            const formattedTime = values.currentTime.format("hh:mm a");
            // combine the dates gathered by moment js and create a timestamp
            updatedValues.date = new Date(`${formattedDate} ${formattedTime}`);
            // console.log(650650, updatedValues.date);

            const extraFields = [];

            // loop through the plant parameters and create the appopriate parameters

            for (let i = 0; i < plant_param_with_spec.length; i++) {
              const param = plant_param_with_spec[i];
              const fieldValue = values[param.parameter];
              if (fieldValue !== undefined && fieldValue !== null) {
                if (param.parameter_type === "BOOLEAN") {
                  extraFields.push({
                    parameter: param.parameter,
                    type: "BOOLEAN",
                    boolean_value: fieldValue,
                  });
                } else if (
                  param.parameter_type === "NUMBER" ||
                  param.parameter_type === "NUMBER_EXACT"
                ) {
                  extraFields.push({
                    parameter: param.parameter,
                    type: "NUMBER",
                    numeric_value: fieldValue,
                  });
                } else if (param.parameter_type === "IMAGE") {
                  // console.log("lets handle image");

                  let ImageURL;

                  if (
                    values[param.parameter] &&
                    values[param.parameter][0] &&
                    !values[param.parameter][0].url
                  ) {
                    //do the uploading
                    ImageURL = await uploadFile({
                      file: values[param.parameter][0].originFileObj,
                      uploadPreset: "inspection_data_images",
                    });

                    extraFields.push({
                      parameter: param.parameter,
                      type: "TEXT",
                      text_value: ImageURL,
                    });
                  } else {
                    // for now lets do nothing
                    // ImageURL = values.profile_photo[0].url;
                  }
                } else if (param.parameter_type === "DATE") {
                  extraFields.push({
                    parameter: param.parameter,
                    type: "DATE",
                    text_value: fieldValue.toDate(), // coz we are getting it from moment js
                  });
                } else if (param.parameter_type === "TEXT") {
                  extraFields.push({
                    parameter: param.parameter,
                    type: "TEXT",
                    text_value: fieldValue,
                  });
                }
              }
            }

            if (extraFields.length === 0) {
              showNotifications({
                type: "error",
                message: "Your Form is Empty",
              });
              return;
            }

            updatedValues.extra_fields = extraFields;

            confirm({
              okText: "Create",
              okType: "danger",
              cancelText: "Cancel",
              title: "Warning!",
              content:
                "You are about to create a new record, this action is irreversible",
              onOk() {
                createProcessInspectionRecord({
                  variables: {
                    data: {
                      ...updatedValues,
                    },
                  },
                });
              },
              onCancel() {},
            });

            setSubmitting(false);
          } catch (e) {
            console.log(e);
          }
        }}
      >
        {({
          values,
          errors,
          touched,
          handleChange,
          handleBlur,
          handleSubmit,
          isSubmitting,
          setFieldValue,
        }) => (
          <form onSubmit={handleSubmit}>
            <div className="date-time-section">
              <Space>
                <DatePicker
                  name="currentDate"
                  allowClear={false}
                  value={values["currentDate"]}
                  onChange={(value, valueString) => {
                    setFieldValue("currentDate", value);
                  }}
                />
                <TimePicker
                  name="currentTime"
                  use12Hours
                  allowClear={false}
                  onChange={(value, timeString) => {
                    setFieldValue("currentTime", value);
                    // if there is no value this will throw an error
                    if (value) {
                      const shift = shiftChanger(value.hour());
                      // console.log(shift);
                      setFieldValue("shift", shift);
                    }
                  }}
                  value={values["currentTime"]}
                  // format={"HH:mm a"}
                  // defaultValue={}
                />
                <div className="whitespace-no-wrap">{values.shift}</div>
              </Space>
            </div>
            {/* handle numeric parameters */}
            {numericParams.map((item) => {
              return (
                <div className="form-row" key={item.parameter}>
                  <label htmlFor={item.parameter}> {item.parameter}</label>
                  <>
                    <InputNumber
                      name={item.parameter}
                      value={values[item.parameter]}
                      onBlur={handleBlur}
                      type="number"
                      onChange={(value) => {
                        setFieldValue(item.parameter, value);
                      }}
                      placeholder={item.parameter}
                    />

                    <div className="error-visualizer-box">
                      {
                        //touched[item.parameter] &&
                        <ErrorVisualizer
                          spec={item.specification}
                          parameter_type={item.parameter_type}
                          value={values[item.parameter]}
                        />
                      }
                    </div>
                  </>
                </div>
              );
            })}
            {/* handle text parameters */}
            {textParams.map((item) => {
              return (
                <div className="form-row" key={item.parameter}>
                  <label htmlFor={item.parameter}> {item.parameter}</label>
                  <Input
                    style={{ maxWidth: "200px" }}
                    value={values[item.parameter]}
                    onChange={handleChange}
                    onBlur={handleBlur}
                    name={item.parameter}
                    placeholder={item.parameter}
                  />
                  <div className="error-visualizer-box">
                    {touched[item.parameter] && (
                      // <ErrorVisualizer
                      //   spec={item.specification}
                      //   parameter_type={item.parameter_type}
                      //   value={values[item.parameter]}
                      // text params will not bear specifications
                      // />
                      <></>
                    )}
                  </div>
                </div>
              );
            })}
            {/* handle boolean parameters */}
            {booleanParams.map((item) => {
              // have to do a modification here
              return (
                <div className="form-row" key={item.parameter}>
                  <label htmlFor={item.parameter}> {item.parameter}</label>

                  <Switch
                    name={item.parameter}
                    size="small"
                    checked={values[item.parameter]}
                    onChange={(value) => {
                      setFieldValue(item.parameter, value);
                    }}
                    onBlur={handleBlur}
                  />

                  <span>
                    &nbsp; &nbsp; value:{" "}
                    {values[item.parameter] === null ? (
                      <span className="inactive">"Not set"</span>
                    ) : values[item.parameter] === true ? (
                      <span className="active"> "Ok"</span>
                    ) : (
                      <span> "Not Ok"</span>
                    )}{" "}
                    {values[item.parameter] != null && (
                      <Button
                        size="small"
                        onClick={() => {
                          setFieldValue(item.parameter, null);
                        }}
                      >
                        Clear
                        <CloseOutlined />
                      </Button>
                    )}
                  </span>
                  <span className="clear-value"></span>

                  <div className="error-visualizer-box">
                    {
                      //touched[item.parameter] &&
                      <ErrorVisualizer
                        spec={item.specification}
                        parameter_type={item.parameter_type}
                        value={values[item.parameter]}
                      />
                    }
                  </div>
                </div>
              );
            })}
            {/* TODO... Handle Date params */}
            {dateParams.map((item) => {
              return (
                <div className="form-row" key={item.parameter}>
                  <label htmlFor={item.parameter}> {item.parameter}</label>

                  <DatePicker
                    name={item.parameter}
                    value={values[item.parameter]}
                    onChange={(value, valueString) => {
                      // console.log(300100, values[item.parameter]);
                      // console.log(300100, value);
                      // console.log(300100, item.parameter);

                      setFieldValue(item.parameter, value);
                    }}
                  />
                  <div>{/* no error object for date type */}</div>
                </div>
              );
            })}
            {/* TODO... Handle image params */}
            {imageParams.map((item) => {
              return (
                <div className="form-row" key={item.parameter}>
                  {/* <label htmlFor={item.parameter}> {item.parameter}</label> */}
                  <FormItem
                    key={item.parameter}
                    label={item.parameter}
                    // validateStatus={errors[formItem.name] ? 'error' : 'validating'}
                    // help={errors[formItem.name]}
                    required={false}
                  >
                    <ImgCrop>
                      <Upload
                        maxCount={1}
                        name={item.parameter}
                        accept="image/*"
                        listType="picture-card"
                        fileList={values[item.parameter]}
                        onChange={({ fileList }) => {
                          console.log(54612, fileList.slice(-1));
                          setFieldValue(item.parameter, fileList.slice(-1));
                        }}

                        // onPreview={onPreview}
                      >
                        {"+ Upload"}
                      </Upload>
                    </ImgCrop>
                  </FormItem>
                  <div className="error-visualizer-box">
                    {touched[item.parameter] && (
                      <ErrorVisualizer
                        spec={item.specification}
                        parameter_type={item.parameter_type}
                        value={values[item.parameter]}
                      />
                    )}
                  </div>
                </div>
              );
            })}
            <b>
              <span> Comments </span>
            </b>
            <TextArea
              rows={4}
              name="comment"
              value={values["comment"]}
              onChange={handleChange}
            />
            <br /> <br />
            <Button size="middle" type="primary" htmlType="submit">
              Create Record
            </Button>
          </form>
        )}
      </Formik>
    </FormikStypeWrapper>
  );
}
