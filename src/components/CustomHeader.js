import React from "react";
import styled from "styled-components";
import { ArrowLeftOutlined, PlusOutlined } from "@ant-design/icons";
import { useHistory, Link } from "react-router-dom";
import { Button } from "antd";

const CustomHeaderWrapper = styled.div`
  height: 75px;
  background-color: #3e92ef;

  #header-content {
    padding: 1.2rem 1rem 1rem 1rem;
    display: flex;
    justify-content: space-between;
  }

  #go-back-button {
    background-color: white;
    width: 40px;
    height: 40px;
    border-radius: 2rem;
    display: flex;
    justify-content: center;
    align-items: center;
    font-size: 1.5rem;
    color: #3e92ef;
    font-weight: bolder;
  }

  .function_button {
    background-color: #fff;
    color: #3e92ef;
    border-radius: 5px;
  }
`;

const CustomHeader = () => {
  let history = useHistory();
  return (
    <CustomHeaderWrapper>
      <div id="header-content">
        <div
          id="go-back-button"
          onClick={() => {
            history.push("/");
          }}
        >
          <ArrowLeftOutlined />
        </div>
        <Link to={`/create-new-log`}>
          <Button
            type="primary"
            htmlType="submit"
            className="function_button"
            icon={<PlusOutlined />}
          >
            Add New
          </Button>
        </Link>
      </div>
    </CustomHeaderWrapper>
  );
};

export default CustomHeader;
