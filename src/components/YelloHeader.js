import React from "react";
import styled from "styled-components";

import { useHistory } from "react-router-dom";

import { CloseOutlined } from "@ant-design/icons";

const YelloHeaderwrapper = styled.div`
  height: 75px;
  display: flex;
  align-items: center;
  justify-content: space-around;
  border-bottom: 1px solid gray;
  background-color: #fff;
  h1 {
    font-size: 1.5rem;
    font-weight: bold;
  }

  #close-button {
    font-size: 1.5rem;
    color: #3778be;
    border: 1px solid #3778be;
    padding: 5px 10px;
    border-radius: 2rem;
    margin-left: 1rem;
    margin-top: -0.5rem;
  }
`;

export const YelloHeader = ({ isPackaging, title }) => {
  let history = useHistory();
  let defaultTitle = `${
    isPackaging ? "Packaging" : "Process"
  }  Inspection Form`;
  if (title != null) {
    defaultTitle = title;
  }
  // console.log(title);
  return (
    <YelloHeaderwrapper>
      <h1>{defaultTitle}</h1>
      <div
        id="close-button"
        onClick={() => {
          history.push("/");
        }}
      >
        <CloseOutlined />
      </div>
    </YelloHeaderwrapper>
  );
};
