import React from "react";
import QAODetailContainer from '../containers/QAODetailContainer/qaoDetailContainer'



const QAODetail = (props) => {
    return <div>
    <QAODetailContainer
    id={props.log.id}
    plant= {props.log.plant}
    created_by={props.log.created_by}
  created_at={props.log.created_at}
  title={props.log.title}
  log={props.log.log}
  first_name={props.log.first_name}
  last_name={props.log.last_name}
  profile_photo={props.log.profile_photo}
  role={props.log.role}
  shift={props.log.shift}
    >
    </QAODetailContainer> 
    </div>;
  };

export default QAODetail;
