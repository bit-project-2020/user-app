import React from "react";
import { useQuery } from "@apollo/react-hooks";

import USER_QUERY from "../queries/User";

const GetUserData = ({ id }) => {
  const { loading, error, data } = useQuery(USER_QUERY, {
    variables: {
      where: {
        id,
      },
    },
  });
  // fetch that id
  if (error) {
    return <div> error </div>;
  }
  if (loading) {
    return <div>loading</div>;
  }

  if (data) {
    const { user } = data;
    return (
      <span>
        <span>
          {user.first_name} {user.last_name} ({user.role})
        </span>
      </span>
    );
  }
};

export default GetUserData;
