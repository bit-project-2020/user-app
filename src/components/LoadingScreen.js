import React from 'react'
import { Spin } from 'antd';
import styled from 'styled-components';

const LoadingContainer = styled.div`
  display: flex;
  height: 100vh;
  align-items: center;
  justify-content: center;
  text-align: center;

  h3 {
    margin-top: 10px;
    color: #b3b3b3;
  }
`;

const LoadingScreen = () => (
  <LoadingContainer>
    <div>
      <Spin size="large" />
      <h3>Please wait...</h3>
    </div>
  </LoadingContainer>
);

export default LoadingScreen;
