import React from "react";
import DeviationDetailContainer from '../containers/DeviationDetailContainer'



const QAODetail = (props) => {
    return <div>
    <DeviationDetailContainer
    id={props.log.id}
    value= {props.log.value}
    created_by={props.log.created_by}
  created_at={props.log.created_at}
  specification={props.log.specification}
  unit={props.log.unit}
  first_name={props.log.first_name}
  last_name={props.log.last_name}
  profile_photo={props.log.profile_photo}
  role={props.log.role}
  plant={props.log.plant}
  product_name={props.log.product_name}
    >
    </DeviationDetailContainer> 
    </div>;
  };

export default QAODetail;