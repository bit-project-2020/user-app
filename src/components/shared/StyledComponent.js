import styled from "styled-components";
import { Dropdown } from "antd";

export const TableActions = styled.div`
  margin-bottom: 16px;
  display: flex;

  .search-box-wrapper {
    flex: 1;

    .search-box {
      width: 200px;
    }
  }
`;

export const ContinentImage = styled.img`
  height: 40px;
`;

export const DestinationImage = styled.img`
  height: 40px;
`;

export const ActionDropdown = styled(Dropdown)`
  cursor: pointer;
`;
